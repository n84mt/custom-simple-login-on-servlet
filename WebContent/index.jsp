<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="authentic.domain.LoginUser"%>
<%@ page import="authentic.constant.PermissionType"%>
<!DOCTYPE HTML SYSTEM "about:legacy-compat">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/main.css" />
<title>メイン画面</title>
</head>
<body>
	<jsp:include page="nav.jsp" />
	<article>
		<h1>メイン画面</h1>
		<%
			String message = (String) session.getAttribute("Message");
			String errorMessage = (String) session.getAttribute("ErrorMessage");
			if (message == null)
				message = "";
			if (errorMessage == null)
				errorMessage = "";
		%>
		<p class="message"><%=message%></p>
		<p class="error-message"><%=errorMessage%></p>
		<%
			if (session.getAttribute("LoginUser") != null) {
		%>
		<p>ログインしたすべてのユーザに「A画面」ボタンが見えます.</p>
		<p>あなたが管理者権限を持っていれば「B画面」ボタンが見えます.</p>
		<form class="transition" action="" type="post">
			<input type="submit" name="buttonType" value="A画面" />

			<%
				if (session.getAttribute("LoginUser") != null && ((LoginUser) session.getAttribute("LoginUser"))
							.permissionTypeNameValue().equals(PermissionType.ADMIN.typeNameValue())) {
			%>
			<input type="submit" name="buttonType" value="B画面" />
			<%
				}
			%>
		</form>
		<%
			}
		%>
	</article>
</body>
</html>