<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="authentic.domain.LoginUser"%>
<!DOCTYPE HTML SYSTEM "about:legacy-compat">
<meta charset="UTF-8">
<nav>
	<%
		if (session.getAttribute("LoginUser") != null) {
	%>
	<span class="user-name-label">ユーザ名：：</span><%=((LoginUser) session.getAttribute("LoginUser")).userNameValue()%>
	<span class="permission-label">権限：：</span><%=((LoginUser) session.getAttribute("LoginUser")).permissionTypeNameValue()%>
	<%
		} else {
	%>
	ログインしていません
	<%
		}
	%>
	<a class="link" href="${pageContext.request.contextPath}/LoginPage.jsp">ログイン画面</a>
</nav>