package authentic;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import authentic.constant.ErrorMessage;
import authentic.constant.SessionAttrName;
import authentic.domain.LoginUser;

/**
 * Servlet implementation class AuthenticationService
 */
@WebServlet("/AuthenticationService")
public class AuthenticationService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthenticationService() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(false);
		session.removeAttribute(SessionAttrName.MESSAGE.toString());
		session.removeAttribute(SessionAttrName.ERROR_MESSAGE.toString());
		session.removeAttribute(SessionAttrName.MESSAGE.toString());
		session.removeAttribute(SessionAttrName.LOGIN_USER.toString());

		String strLoginId = request.getParameter("LoginID");
		String strPassword = request.getParameter("Password");
		try {
			LoginUser loginUser = new LoginUser(strLoginId, strPassword);
			session.setAttribute(SessionAttrName.MESSAGE.toString(), "ログインしました.");
			session.setAttribute("LoginUser", loginUser);
		} catch (IllegalArgumentException e) {
			session.setAttribute(SessionAttrName.ERROR_MESSAGE.toString(), e.getMessage());
		} catch (SQLException e) {
			session.setAttribute(SessionAttrName.ERROR_MESSAGE.toString(), ErrorMessage.SQL_EXCEPTION.toString());
			e.printStackTrace();
		} catch (Exception e) {
			session.setAttribute(SessionAttrName.ERROR_MESSAGE.toString(), ErrorMessage.EXCEPTION.toString());
			e.printStackTrace();
		}

		response.sendRedirect("index.jsp");
	}

}
