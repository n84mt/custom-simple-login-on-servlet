package authentic.constant;

public enum ErrorMessage {
	SQL_EXCEPTION("データベースへの問い合わせに失敗しました"),
	EXCEPTION("システムエラーが発生しました");

    private final String message;

    //コンストラクタ
    ErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
