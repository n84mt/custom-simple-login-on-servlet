package authentic.constant;

public enum PermissionType {
	READ_ONLY("読込専用", "1"), ADMIN("管理者", "2");

	private final String typeName;
	private final String strStatus;

	PermissionType(final String typeName, final String strStatus) {
		this.typeName = typeName;
		this.strStatus = strStatus;
	}

	public String typeNameValue() {
		return this.typeName;
	}

	public String strStatusValue() {
		return this.strStatus;
	}

	public static String getTypeNameFromStatus(String strStatus) {
		String typeName = new String();
		for (PermissionType permissionType : PermissionType.values()) {
			if (permissionType.strStatusValue().equals(strStatus)) {
				typeName = permissionType.typeNameValue();
				break;
			}
		}
		return typeName;
	}
}
