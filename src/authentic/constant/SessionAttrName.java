package authentic.constant;

public enum SessionAttrName {
    MESSAGE("Message"),
    ERROR_MESSAGE("ErrorMessage"),
	LOGIN_USER("LoginUser");

    private final String name;

    //コンストラクタ
    SessionAttrName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
