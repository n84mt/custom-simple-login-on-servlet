package authentic.domain;

public class LoginID {
	private String loginId;
	private static final int MIN_LENGTH = 1;
	private static final int MAX_LENGTH = 8;
	private static final String NO_LOGIN_ID_ERROR_MESSAGE = "Login ID が入力されていません";
	private static final String OVER_LOGIN_ID_LENGTH_ERROR_MESSAGE = "Login ID が8文字を超過しています";

	public LoginID(String loginId) {
		if (loginId.length() < MIN_LENGTH)
			throw new IllegalArgumentException(NO_LOGIN_ID_ERROR_MESSAGE);
		if (MAX_LENGTH < loginId.length())
			throw new IllegalArgumentException(OVER_LOGIN_ID_LENGTH_ERROR_MESSAGE);
		this.loginId = loginId;
	}

	@Override
	public String toString() {
		return this.loginId;
	}
}
