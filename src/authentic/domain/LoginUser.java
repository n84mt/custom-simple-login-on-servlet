package authentic.domain;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import authentic.constant.PermissionType;
import authentic.dao.DBManager;

public class LoginUser {
	private LoginID loginId;
	private Password password;
	private UserName userName;
	private Permission permission;
	private static final String THIS_USER_IS_NOT_EXIST_ERROR_MESSAGE = "不正：入力されたユーザは存在しません";
	private static final String THIS_USER_HAS_MANY_RECORDS_ERROR_MESSAGE = "不正：ユーザが複数存在しています";
	private static final String THIS_PASSWORD_IS_WRONG_ERROR_MESSAGE = "不正：パスワードが違います";

	public LoginUser(String strLoginId, String strPassword) throws IllegalArgumentException, SQLException, Exception {
		this.loginId = new LoginID(strLoginId);
		this.password = new Password(strPassword);

		if (isExistThisUserRecord(strLoginId) && isCorrectThisPassword(strLoginId, strPassword)) {
			this.userName = getUserName(strLoginId, strPassword);
			this.permission = getPermission(strLoginId, strPassword);
		}
	}

	private boolean isExistThisUserRecord(String strLoginId) throws SQLException, Exception {
		DBManager db = new DBManager();
		String sql = "SELECT count(*) AS total FROM m_user WHERE login_id = ?;";
		PreparedStatement statement = db.getPreparedStatement(sql);
		statement.setString(1, strLoginId);
		ResultSet rs = statement.executeQuery();
		int intTotal = 0;
		while (rs.next()) {
			intTotal = rs.getInt("total");
		}
		db.close();
		if (intTotal < 1)
			throw new IllegalArgumentException(THIS_USER_IS_NOT_EXIST_ERROR_MESSAGE);
		if (1 < intTotal)
			throw new IllegalArgumentException(THIS_USER_HAS_MANY_RECORDS_ERROR_MESSAGE);
		return true;
	}

	private boolean isCorrectThisPassword(String strLoginId, String strPassword) throws SQLException, Exception {
		DBManager db = new DBManager();
		String sql = "SELECT count(*) AS total FROM m_user WHERE login_id = ? AND password = ? GROUP BY login_id, password;";
		PreparedStatement statement = db.getPreparedStatement(sql);
		statement.setString(1, strLoginId);
		statement.setString(2, strPassword);
		ResultSet rs = statement.executeQuery();
		int intTotal = 0;
		while (rs.next()) {
			intTotal = rs.getInt("total");
		}
		db.close();
		if (intTotal < 1)
			throw new IllegalArgumentException(THIS_PASSWORD_IS_WRONG_ERROR_MESSAGE);
		return true;
	}

	private UserName getUserName(String strLoginId, String strPassword) throws SQLException, Exception {
		DBManager db = new DBManager();
		String sql = "SELECT user_name FROM m_user WHERE login_id = ? AND password = ? GROUP BY login_id, password;";
		PreparedStatement statement = db.getPreparedStatement(sql);
		statement.setString(1, strLoginId);
		statement.setString(2, strPassword);
		ResultSet rs = statement.executeQuery();
		String strPermission = null;
		while (rs.next()) {
			strPermission = rs.getString("user_name");
		}
		db.close();
		return new UserName(strPermission);
	}

	private Permission getPermission(String loginId, String password) throws SQLException, Exception {
		DBManager db = new DBManager();
		String sql = "SELECT permission FROM m_user WHERE login_id = ? AND password = ? GROUP BY login_id, password;";
		PreparedStatement statement = db.getPreparedStatement(sql);
		statement.setString(1, loginId);
		statement.setString(2, password);
		ResultSet rs = statement.executeQuery();
		String strPermission = null;
		while (rs.next()) {
			strPermission = rs.getString("permission");
		}
		db.close();
		return new Permission(strPermission);
	}

	public String loginIdValue() {
		return this.loginId.toString();
	}

	public String passwordValue() {
		return this.password.toString();
	}

	public String userNameValue() {
		return this.userName.toString();
	}

	public String permissionTypeNameValue() {
		return PermissionType.getTypeNameFromStatus(this.permission.toString());
	}
}
