package authentic.domain;

public class Password {
	String password;
	private static final int MIN_LENGTH = 1;
	private static final int MAX_LENGTH = 8;
	private static final String NO_PASSWORD_ERROR_MESSAGE = "パスワード が入力されていません";
	private static final String OVER_PASSWORD_LENGTH_ERROR_MESSAGE = "パスワードが8文字を超過しています";

	public Password(String password) {
		if (password.length() < MIN_LENGTH)
			throw new IllegalArgumentException(NO_PASSWORD_ERROR_MESSAGE);
		if (MAX_LENGTH < password.length())
			throw new IllegalArgumentException(OVER_PASSWORD_LENGTH_ERROR_MESSAGE);
		this.password = password;
	}

	@Override
	public String toString() {
		return this.password;
	}
}
