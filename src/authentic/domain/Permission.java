package authentic.domain;

public class Permission {
	String permission;
	private static final int MIN_LENGTH = 1;
	private static final int MAX_LENGTH = 8;
	private static final String NO_PERMISSION_ERROR_MESSAGE = "不正：権限を所持していません";
	private static final String PERMISSION_LENGTH_ERROR_MESSAGE = "不正：存在しない権限を所持しています";

	public Permission(String permissionStatus) {
		if (permissionStatus.length() < MIN_LENGTH)
			throw new IllegalArgumentException(NO_PERMISSION_ERROR_MESSAGE);
		if (MAX_LENGTH < permissionStatus.length())
			throw new IllegalArgumentException(PERMISSION_LENGTH_ERROR_MESSAGE);
		this.permission = permissionStatus;
	}

	 @Override
	 public String toString() {
	 return this.permission;
	 }
}
