package authentic.domain;

public class UserName {
	String userName;
	private static final int MIN_LENGTH = 1;
	private static final int MAX_LENGTH = 20;
	private static final String NOT_RESISTED_USER_NAME_ERROR_MESSAGE = "不正：ユーザ名が登録されていません";
	private static final String OVER_USER_NAME__LENGTH_ERROR_MESSAGE = "不正：ユーザ名が20文字を超過しています";

	public UserName(String userName) {
		if (userName.length() < MIN_LENGTH)
			throw new IllegalArgumentException(NOT_RESISTED_USER_NAME_ERROR_MESSAGE);
		if (MAX_LENGTH < userName.length())
			throw new IllegalArgumentException(OVER_USER_NAME__LENGTH_ERROR_MESSAGE);
		this.userName = userName;
	}

	@Override
	public String toString() {
		return this.userName;
	}
}
